package com.silalahi.valentinus.gitlab.training.backendpayment.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

@Configuration
@EnableWebSecurity
@EnableResourceServer
public class KonfigurasiSecurity extends ResourceServerConfigurerAdapter {

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/**").hasAuthority("CONFIGURE_SYSTEM");
	}

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		RemoteTokenServices tokenService = new RemoteTokenServices();
		tokenService.setClientId("client001");
		tokenService.setClientSecret("abcd");
		tokenService.setCheckTokenEndpointUrl("http://localhost:8080/oauth/check_token");

		resources.resourceId("belajar").tokenServices(tokenService);
	}

}
