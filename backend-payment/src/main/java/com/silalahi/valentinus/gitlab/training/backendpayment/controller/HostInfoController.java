package com.silalahi.valentinus.gitlab.training.backendpayment.controller;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class HostInfoController {
	public Map<String, String> info() {
		Map<String, String> data = new HashMap<>();
		data.put("waktu", LocalDateTime.now().toString());
		return data;
	}
}
